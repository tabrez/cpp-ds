#include<iostream>
#include<cstdlib>

int sequential_search(int a[], int l, int r, int v)
{
  for(int i = l; i < r; ++i)
    if(a[i] == v) return i;
  return -1;
}

int binary_search(int a[], int l, int r, int v)
{
  while(r >= l)
    {
      int m = (l+r)/2;    // mid: l + (r-l)/2
      if(v == a[m]) return m;
      if(v < a[m])
        r = m-1;
      else
        l = m+1;
    }
  return -1;
}

// Sample data
int a[] = { 3, 4, 1, 9, 12, 7, 21, -14, 5, 121, 78, 82, 11, 8, 2, 64, 109, 77, 43, 29, 128, 21, 83, 44 };
const int size = sizeof(a)/sizeof(a[0]);

int check_correctness_ss()
{
  int r = sequential_search(a, 0, size-1, -14);//sequential_search(a, 0, 9, -14); 
  return (r == 7) ? true : false;
}

int check_correctness_bs()
{
  int r = binary_search(a, 0, size-1, -14);
  return (r == 7) ? true : false;
}

void driver()
{
  int size = 1000;
  int *arr = new int[size];

  //initialize array with random numbers
  srand(10);
  for(int i=0; i<size; ++i)
    arr[i] = rand()%100;

  int trial_count = 20;
  for(int i=0; i<trial_count; ++i)
    {
      int v = rand()%100;
      std::cout << "Value: " << v << " has first occurence at: " << sequential_search(arr, 0, size, v) << std::endl;
    }
  delete [] arr;
}

int main()
{
  std::cout << "Index of " << 12 << " is: " << sequential_search(a, 0, 9, 12) << std::endl;

  //bool r = check_correctness_ss();
  //std::cout << "Correct? : " << r << std::endl;

  driver();

  std::cout << "End of program." << std::endl;
}
