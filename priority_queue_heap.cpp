#include <algorithm>
#include <iostream>

void fixUp(int a[], int k)
{
  while (k > 1 && a[k/2] < a[k])
    {
      std::swap(a[k], a[k/2]);
      k = k/2;
    }
}

void fixDown(int a[], int k, int N)
{
  while(2*k <= N)
    {
      int j = 2*k;
      if(j < N && a[j] < a[j+1])
        j++;
      if(!(a[k] < a[j]))
        break;
      std::swap(a[k], a[j]);
      k = j;
    }
}

class priority_queue_heap
{
public:
  priority_queue_heap(int n)
  {
    queue = new int[n+1];
    i = 0;
  }

  bool empty()
  {
    return i == 0;
  }

  void insert(int v)
  {
    queue[++i] = v;
    fixUp(queue, i);
  }

  int remove_max()
  {
    std::swap(queue[1], queue[i]);
    fixDown(queue ,1,i-1);
    return queue[i--];
  }

  private:
  int *queue;
  int i;
};

void heapsort(int a[], int n)
{
  int *pq = a-1;
  // Heapify
  for(int k=n/2; k >= 1; --k)
    fixDown(pq, k, n);

  // Move max to end of array
  for (int k=n; k > 1;)
  {
    std::swap(pq[1], pq[k]);
    --k;
    fixDown(pq, 1, k);  // or --k
  }
}

int main()
{
  priority_queue_heap pqh(100);
  pqh.insert(100);
  pqh.insert(-101);
  pqh.insert(99);
  pqh.insert(-23);
  std::cout << pqh.remove_max() << std::endl;
  std::cout << pqh.remove_max() << std::endl;
  std::cout << pqh.remove_max() << std::endl;
  std::cout << pqh.remove_max() << std::endl;

  std::cout << "########################" << std::endl;

  for(int j = -77; j <=0; ++j)
    pqh.insert(j);
  for(int k = 0; k < 77; ++k)
    std::cout << pqh.remove_max() << std::endl;

  std::cout << "########################" << std::endl;

  int arr[10];
  for(int i = 0; i < 10; ++i)
   arr[i] = 10-i;

  heapsort(arr, 10);
  for(int i = 0; i < 10; ++i)
   std::cout << arr[i] << std::endl;

  std::cout << "########################" << std::endl;

  int arr2[] = {23, -90, 39, 99, 101, -20};
  //int arr2[] = {6,5,4,3,2,1};
  heapsort(arr2, 6);
  for(int i = 0; i < 6; ++i)
    std::cout << arr2[i] << std::endl;

  std::cout << "########################" << std::endl;
}
