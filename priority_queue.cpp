#include <algorithm>
#include <iostream>

int max_index(int a[], int size)
{
  int max = 0;
  for(int i = 1; i < size; ++i)
    if(a[i] > a[max])
      max = i;
  return max;
}

class priority_queue
{
public:
  priority_queue(int n)
  {
    queue = new int[n];
    // size=n
    i = 0;
  }

  bool empty(){ return i == 0; }

  void insert(int v)
  {
    // full?
    queue[i++] = v;
    //i++
  }

  int remove_max()
  {
    // empty?
    int mi = max_index(queue, i);
    std::swap(queue[mi], queue[i-1]);
    return queue[--i];
  }

private:
  int i;
  int *queue;
};

// inefficient
void priority_queue_sort(int a[], int n)
{
  priority_queue pq(n);
  for(int i = 0; i < n; i++)
    pq.insert(a[i]);
  for(int i = n - 1; i >=0; i--)
    a[i] = pq.remove_max();
}

int main()
{
  priority_queue pq(100);
  pq.insert(100);
  pq.insert(-101);
  pq.insert(99);
  pq.insert(-23);
  std::cout << pq.remove_max() << std::endl;
  std::cout << pq.remove_max() << std::endl;
  std::cout << pq.remove_max() << std::endl;
  std::cout << pq.remove_max() << std::endl;

  std::cout << "########################" << std::endl;

  for(int j = -77; j <=0; ++j)
    pq.insert(j);
  for(int k = 0; k < 77; ++k)
    std::cout << pq.remove_max() << std::endl;

  std::cout << "########################" << std::endl;

  int arr[10];
  for(int i = 0; i < 10; ++i)
   arr[i] = 10-i;

  priority_queue_sort(arr, 10);
  for(int i = 0; i < 10; ++i)
   std::cout << arr[i] << std::endl;

  std::cout << "########################" << std::endl;

  int arr2[] = {23, -90, 39, 99, 101, -20};
  //int arr2[] = {6,5,4,3,2,1};
  priority_queue_sort(arr2, 6);
  for(int i = 0; i < 6; ++i)
    std::cout << arr2[i] << std::endl;

  std::cout << "########################" << std::endl;
}
