#include <algorithm>
#include <iostream>

void merge(int a[], int left, int middle, int right)
{
  int * c = new int[right - left + 1];
  for(int i = left, j = middle+1, k = 0; k < right - left + 1; ++k)
  {
    if(i == middle+1)
    {
      c[k] = a[j++];
      continue;
    }
    if(j == right+1)
    {
      c[k] = a[i++];
      continue;
    }
    c[k] = a[i] < a[j] ? a[i++] : a[j++];
  }
  for(int k = left; k <= right; ++k)
    a[k] = c[k-left];

   delete [] c;
}

void merge2(int a[], int a_size, int b[], int b_size )
{
  int *c = new int[a_size+b_size];
  for(int i = 0, j = 0, k = 0; k < a_size+b_size; ++k)
  {
    if(i == a_size)
    {
      c[k] = b[j++];
      continue;
    }
    if(j == b_size)
    {
      c[k] = a[i++];
      continue;
    }
    c[k] = a[i] < b[j] ? a[i++] : b[j++];
  }

}

void merge_sort(int a[], int left, int right)
{
  if(left >= right) return;
  int middle = (right + left) / 2;
  merge_sort(a, left, middle);
  merge_sort(a, middle+1, right);
  merge(a, left, middle, right);
}


int main()
{
  int a[] = {8, 3, -1, 25, -90, 1000, 250, -21, 120};
  merge_sort(a, 0, 8);
  for(int i = 0 ; i < 9; ++i)
    std::cout << a[i] << std::endl;
}
