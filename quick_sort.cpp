#include <iostream>
#include <algorithm>

int partition(int a[], int left, int right)
{
  int i = left-1, j = right, v = a[right];

  while(true)
  {
    while(a[++i] < v) continue;
    while(a[--j] > v) if(j == left) break;
    if(i >= j) break;
    std::swap(a[i], a[j]);
  }
  std::swap(a[i], a[right]);
  return i;
}

void quick_sort(int a[], int left, int right)
{
  if(left >= right) return;
  int i = partition(a, left, right);
  quick_sort(a, left, i-1);
  quick_sort(a, i+1, right);
}

int main()
{
  int a[] = {8, 3, -1, 25, -90, 1000, 250, -21, 120};
  quick_sort(a, 0, 8);
  for(int i = 0 ; i < 9; ++i)
    std::cout << a[i] << std::endl;
}

