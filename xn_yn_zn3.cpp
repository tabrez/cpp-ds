#include<iostream>
#include<cmath>
#include<vector>
//using namespace std;

class Triple           // Or use std::tuple from #include<utility>
{
public:
  int x; int y; int z;
  Triple() { x = y = z = 0; }
  Triple(int lx, int ly, int lz) { x=lx; y=ly; z=lz; }
  friend bool operator==( Triple t1,  Triple t2)
  {
    return t1.x == t2.x && t1.y == t2.y && t1.z == t2.z;
  }
};
typedef std::vector<Triple> ListTriple;

class XYZN_Problem
{
public:
  XYZN_Problem() { bruteforce=false; comparisons=0; }
  ListTriple results() { return _results; }
  void clear() { _results.clear(); comparisons=0; }

  bool check_correctness()
  {
    find_xyz();
    ListTriple a = results();
    find_xyz_brute_force();
    ListTriple b = results();
    return (a == b);
  }

  void find_xyz_brute_force()
  {
    bruteforce = true;
    find_xyz();
    bruteforce = false;
  }

  void find_xyz()
  {
    clear();
    for(int z=1; z <= limit; ++z)
      helper_find_xyz(z);
  }

  void helper_find_xyz(int z)
  {
    for(int x=1; x <= limit; ++x)
      for(int y=1; y <= limit; ++y)
        {
          comparisons++;
          int xyn = pow(x, n) + pow(y, n);
          int zn = pow(z, n);
          if(xyn == zn) _results.push_back(Triple(x,y,z));

          if(!bruteforce && xyn >= zn)
            {
              //if(x >= z) return;
              break;
            }
        }
  }

  void print_results()
  {
    //for(size_t i=0; i < _results.size(); ++i)
    //std::cout << "[ " << _results[i].x << ", " << _results[i].y << ", " << _results[i].z << "]" << std::endl;
    std::cout << "Total comparisons: " << comparisons << std::endl;
    std::cout << "Total results: " << _results.size() << std::endl;
  }

private:
  ListTriple _results;
  static const int limit = 1000;
  static const int n = 2;
  bool bruteforce;
  int comparisons;
};

int main()
{
  //int n=1;
  //int limit = 1000;

  XYZN_Problem xyzn;
  //std::cout << (xyzn.check_correctness() ? "Correct" : "Incorrect") << std::endl;
  //xyzn.find_xyz_brute_force();
  //xyzn.print_results();
  xyzn.find_xyz();
  xyzn.print_results();
}
