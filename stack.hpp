template<typename T>
class stack
{
public:
	stack(int n) { size=n; a = new T[size]; head=0; }
	~stack() { delete [] a; }

	void push(T v) 
	{ 
		if(full()) return;
		a[head] = v;
		head++;
	}

	bool pop(T &v)
	{
		if(empty()) return false; 
		v = a[head-1];
		--head;
		return true;
	}

	bool full()
	{
		return head == size;
	}

	bool empty()
	{
		return head == 0;
	}

private:
	T *a;
	int size;
	int head;
};
