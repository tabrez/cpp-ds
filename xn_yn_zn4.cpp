#include <iostream>
#include <vector>

const long UPTO = 100000;

struct num
{
  num():x(0),y(0),z(0){}
  num(long x_, long y_, long z_):x(x_),y(y_),z(z_){}
  long x,y,z;
};
long xyz(std::vector<num> & nums)
{
  long count = 0;
  long k =0;
  for(long i = 1; i <= UPTO; ++i)
    {
      if(i > k) k = i;
      for(long j = 1, k = i; j <= UPTO; ++j)
        {
          if(j > k) k = j;

          for ( ; k <= UPTO; ++k)
            {
              ++count;
              long sum = i*i + j*j;
              if(sum < k*k)
                break;
              if(sum == k*k)
                nums.push_back(num(i,j,k));
            }
        }
    }
  return count;
}


int main()
{
  std::vector<num> nums;
  long count = xyz(nums);
  //for(std::vector<num>::iterator i = nums.begin(); i != nums.end(); ++i)
  //  std::cout << i->x << " " << i->y << " " << i->z << std::endl;
  std::cout << "matches = " << nums.size() << " " << "comparisions = " << count << std::endl;
}

