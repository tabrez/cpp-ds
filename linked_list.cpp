#include<iostream>

// 1a. Linked List Definition
struct node
{
  int item;     //or:  T item;
  node *next;

  // 1b. Constructor
  node(int e=0, node *n=0)
  {
    item = e;
    next = n;
  }
};

typedef node* link;

struct linked_list
{
  link head;
  // link tail;
  int size;
  linked_list(link n=0) { head = n; size = 0; }
};

typedef linked_list* link_list;

int main()
{
  // Example: Creating a linked list
  link l1 = new node(17, NULL);  // node *l1 = new node(17, 0);   l1 -> [ 17, null ] ; null, nil  ;;   l1 = new node(); l1.item = 17; l1.next = 0;
  link l2 = new node(33, l1); /* l2 -> [ 33,  (.) ]
                                               ^
                                               |
                                               ---> [ 17, 0 ]     */
  link l3 = new node(45, l2);  // [45, (.) ] -> [ 33, (.) ] -> [ 17, null]
                               // ^              ^              ^
                               // l3            l2             l1


  /*
    l1->next is null.
    l2->next is the same as l1.
    l3->next is the same as l2.
    Only l3 is needed to traverse the entire list, l2 & l1 are unnecessary.
  */

  // 2. Linked List Traversal
  linked_list *ll = new linked_list(0);
  // add nodes to ll
  ll->head = l3;
  for(link t = ll->head; t != 0; t = t->next)    //  t->next != head
    {
      // use t
      std::cout << t->item << " ";
    }

  // 3. Manipulating a List
  // 3a. Remove: l2
  link t = l3->next;
  l3->next = l3->next->next; //or: l3->next = l2->next
  delete t;

  // 3b. Insert: after l3
  link t2 = new node(92, 0);
  t2->next = l3->next;
  l3->next = t2;
}

//3c. Next
link next(link n)
{
  if(n == 0) return 0;
  return n->next;
}

// 3c. Empty
bool empty(linked_list *ll)
{
  return ll->head == 0;
}

// 3d. Reverse: n
link reverse(link n)
{
  link nn = n;
  link r = 0;
  while(nn != 0)
    {
      link t = nn->next;
      nn->next = r;
      r = nn;
      nn = t;
    }
  return r;
}
