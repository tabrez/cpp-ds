#include<iostream>
#include<cmath>

int main()
{
  int n = 2;
  int comparisons = 0;
  int results = 0;
  const int limit = 100;

  for(int z=1; z <= limit; ++z)
    for(int x=1; x <= limit; ++x)
      for(int y=1; y <= limit; ++y)
        {
          comparisons++;
          int xyn = pow(x, n) + pow(y, n);
          int zn = pow(z, n);
           // Or store in a variable
          if(xyn == zn)
            {
              results++;
              std::cout << "[ " << x << ", " << y << ", " << z << "]" << std::endl;
            }
        }
  std::cout << "Comparisons: " << comparisons << std::endl;
  std::cout << "Results: " << results << std::endl;
}
