#include<iostream>
#include<algorithm>
#include"queue.hpp"
#include"stack.hpp"

struct node
{
	int item;
	node *left;
	node *right;
	node(int v=0, node *l=0, node *r=0) 
	{ 
		item=v; 
		left=l; 
		right=r; 
	}
};

typedef node *tlink;

void print_node(tlink node) { std::cout << "[" << node->item << "] "; }

class binary_tree
{
public:
	binary_tree() { root = 0; }
	binary_tree(int v) { root = new node(v); }
	binary_tree(int a[], int size)
	{
		construct(a, size);
	}
	void construct(int a[], int size)
	{
		//destruct(root);
		queue<tlink> q(1000);
		
		root = new node();
		tlink n = root;

		q.push(n);		
		for(int i=1; i <= size; ++i)
		{
			if(q.pop(n))
				n->item = a[i-1];
			if(i > size/2) continue;
			if(n->left == 0) { n->left = new node(); q.push(n->left); }
			if(n->right == 0 && !(size%2 == 0 && i == size/2)) { n->right = new node(); q.push(n->right); }
		}
	}
	~binary_tree()
	{
		destruct(root);
	}
	void destruct(tlink node)
	{
		if(node == 0) return;
		destruct(node->left);
		destruct(node->right);
		delete node;
	}

	// Traversal: Pre-order recursive version
	void traverse(void (*visit)(tlink))
	{
		traverse_(root, visit);
	}

	void traverse_(tlink node, void (*visit)(tlink))
	{
		if(node == 0) return;
		visit(node);
		traverse_(node->left, visit);
		traverse_(node->right, visit);
	}

	// Traversal: Pre-order using a stack/iterative version
	void traverse2(void (*visit)(tlink))
	{
		stack<tlink> s(1000);
		tlink n = root;
		s.push(n);
		while(!s.empty())
		{
			s.pop(n);
			visit(n);
			if(!n->right) s.push(n->right);
			if(!n->left) s.push(n->left);
		}
	}

	//Traversal: Level-order using a queue
	void traverse3(void (*visit)(tlink))
	{
		queue<tlink> q(1000);
		tlink n = root;
		q.push(n);
		while(!q.empty())
		{
			q.pop(n);
			visit(n);
			if(n->left) q.push(n->left);
			if(n->right) q.push(n->right);
		}
	}

	int get_count()
	{
		return count_(root);
	}
	int count_(tlink node)
	{
		if(node == 0) return 0;
		return count_(node->left) + count_(node->right) + 1;
	}

	int height()
	{
		return height_(root);
	}
	int height_(tlink node)
	{
		if(node == 0) return 0;
		return std::max(height_(node->left), height_(node->right)) + 1;
	}

	void print_tree() { traverse3(print_node); }

	// Assignment: count and return number of leaf nodes in the tree
	// Assignment: show an alternative version of Binary Tree representation
private:
	int count;		
	tlink root;
};

int main()
{
	int array[] = { 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13 };
	binary_tree t1(array, 13);		
	t1.print_tree();
	std::cout << std::endl << t1.get_count();
	std::cout << std::endl << t1.height();	 
	std::cout << std::endl << "End of program." << std::endl;
}
