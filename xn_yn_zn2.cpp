#include<iostream>
#include<cmath>
#include<vector>
//using namespace std;

class Triple           // Or use std::tuple from #include<utility>
{
public:
  int x; int y; int z;
  Triple() { x = y = z = 0; }
  Triple(int lx, int ly, int lz) { x=lx; y=ly; z=lz; }
};
typedef std::vector<Triple> ListTriple;

int comparisons;

ListTriple find_xyz(int n)
{
  comparisons = 0;
  const int limit = 10;
  ListTriple results;

  int z=1;
  NEXT_Z:
  while(z++ <= limit)
    for(int x=1; x <= limit; ++x)
      for(int y=1; y <= limit; ++y)
        {
          comparisons++;
          int zn = pow(z, n);
          int xyn = pow(x, n) + pow(y, n);

          //if(x >= z) goto NEXT_Z;
          //if(xyn > zn) break;

          if(xyn == zn) results.push_back(Triple(x,y,z));
        }
  return results;
}

void print_results(ListTriple vec)
{
  for(size_t i=0; i < vec.size(); ++i)
    std::cout << "[ " << vec[i].x << ", " << vec[i].y << ", " << vec[i].z << "]" << std::endl;
  std::cout << "Results: " << vec.size() << std::endl;
  std::cout << "Comparisons: " << comparisons << std::endl;
}

int main()
{
  int n=2;
  ListTriple results = find_xyz(n);
  print_results(results);
}
