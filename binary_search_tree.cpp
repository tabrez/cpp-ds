#include <iostream>

struct node
{
	int item;
	node *left;
	node *right;
	node(int v=0, node *l=0, node *r=0) 
	{ 
		item=v; 
		left=l; 
		right=r; 
	}
};

typedef node *tlink;

// balanced binary search tree
class binary_search_tree
{
public:
	binary_search_tree(tlink node = 0) { root = node; }
	binary_search_tree(int v) { root = new node(v); }
	~binary_search_tree()
	{
		destruct(root);
	}
	void destruct(tlink node)
	{
		if(node == 0) return;
		destruct(node->left);
		destruct(node->right);
		delete node;
	}

	tlink get_root() { return root; }
	void set_root(tlink node) { root = node; }

	bool search(int v)
	{
		return search_(v, root);
	}

	bool search_(int v, tlink node)
	{
		if(node == 0) return false;
		if(v == node->item) return true;
		if(v < node->item) return search_(v, node->left);
		return search_(v, node->right);
	}

  // insert without balancing the tree
	void insert(int v)
	{
		insert_(v, root);
	}

	tlink insert_(int v, tlink& nodep)
	{
		if(nodep == 0) 
			nodep = new node(v);			
		else if(v < nodep->item)
			insert_(v, nodep->left);
		else
			insert_(v, nodep->right);
	}

	void rotate_ll(tlink& a)
	{
		tlink al = a->left;

		a->left = al->right;
		al->right = a;
		a = al;
	}

	void rotate_rr(tlink& a)
	{
		tlink ar = a->right;

		a->right = ar->left;
		ar->left = a;
		a = ar;
	}

  // insert and balance the tree
	void root_insert(int v)
	{
		root_insert_(v, root);
	}

	void root_insert_(int v, tlink& nodep)
	{
		if(nodep == 0)
			nodep = new node(v);
		else if(v < nodep->item)
		{
			root_insert_(v, nodep->left);
			rotate_ll(nodep);
		}
		else
		{
			root_insert_(v, nodep->right);
			rotate_rr(nodep);
		}
	}

	void print()
	{
		print_(root);
	}

	void print_(tlink node)
	{
		if(node == 0) return;
		print_(node->left);
		std::cout << node->item << std::endl;
		print_(node->right);
	}

	void join(tlink b)
	{
		root = join_(root, b);
	}

	tlink join_(tlink a, tlink b)
	{
		if(a == 0) return b;
		if(b == 0) return a;
		binary_search_tree bt;
		bt.root_insert_(b->item, a);
		a->left = join_(a->left, b->left);
		a->right = join_(a->right, b->right);
		return a;
	}

	void remove(int v)
	{
		remove_(v, root);
	}

	void remove_(int v, tlink& node)
	{
		if(node == 0) return;
		if(v < node->item) remove_(v, node->left);
		if(v > node->item) remove_(v, node->right);
		if(v == node->item) 
		{
			tlink t = node;
			node = join_(node->left, node->right);
			delete t;
		}
	}

private:
	tlink root;
};



int main()
{
	binary_search_tree bst1;
	bst1.insert(10);
	bst1.insert(20);
	bst1.insert(30);
	
	bst1.insert(110);
	bst1.insert(201);
	bst1.insert(310);

	bst1.insert(1220);
	bst1.insert(2011);
	bst1.insert(2130);

	bst1.insert(101);
	bst1.insert(202);
	bst1.insert(303);

	bst1.remove(101);
	bst1.remove(201);

	bst1.print();
	std::cout << "**************************************" << std::endl;

	binary_search_tree bst2;
	bst2.root_insert(101);
	bst2.root_insert(201);
	bst2.root_insert(301);
	
	bst2.root_insert(1101);
	bst2.root_insert(2011);
	bst2.root_insert(3101);

	bst2.root_insert(12201);
	bst2.root_insert(20111);
	bst2.root_insert(21301);

	bst2.root_insert(1011);
	bst2.root_insert(2021);
	bst2.root_insert(3031);

	bst2.remove(1011);
	bst2.remove(3031);
	
	bst2.print();

	std::cout << "**************************************" << std::endl;

	bst1.join(bst2.get_root());
	bst2.set_root(0);
	bst1.print();
	std::cout << "**************************************" << std::endl;


	std::cout << std::endl;
	std::cout << "search 20: " << std::endl;
	std::cout << bst1.search(20) << std::endl;
	std::cout << " search 100: " << std::endl;
	std::cout << bst1.search(100) << std::endl;

	

}