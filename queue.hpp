template<typename T>
class queue
{
public:
	queue(int n) { size=n; a = new T[size]; head=0; tail=0; }
	~queue() { delete [] a; }

	void push(T v) 
	{ 
		if(full()) return;
		a[tail] = v;
		tail = incr(tail);
	}

	bool pop(T &v)
	{
		if(empty()) return false; 
		v = a[head];
		head = incr(head);
		return true;
	}

	bool full()
	{
		return head == incr(tail);
	}

	bool empty()
	{
		return head == tail;
	}

private:
	T *a;
	int size;
	int head;
	int tail;

	int incr(int x) { return x == size-1 ? 0 : x+1; }
};
