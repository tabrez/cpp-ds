#include<iostream>
#include"queue.hpp"
#include"stack.hpp"

int main()
{
	// Testing queue
	queue<int> q(100);
	q.push(1);
	q.push(2);
	q.push(3);
	q.push(4);
	q.push(5);
	q.push(6);

	std::cout << "Popping all items in the queue:" << std::endl;
	int a;
	while(q.empty() != true)
	{
		q.pop(a);
		std::cout << a << " ";
	}

	// Testing stack
	stack<int> s(100);
	s.push(1);
	s.push(2);
	s.push(3);
	s.push(4);
	s.push(5);
	s.push(6);

	std::cout << std::endl << "Popping all items in the stack:" << std::endl;
	int b;
	while(s.empty() != true)
	{
		s.pop(b);
		std::cout << b << " ";
	}

	std::cout << std::endl;
}
